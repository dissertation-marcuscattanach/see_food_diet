package com.see_food_diet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="recipes")
public class Recipe {
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer recipe_id;
	
	@Column
	private String name; 
	
	@Column
	private String ingredients;
	
//	public enum Ingredients{
//		GARLIC, MINCE, PASSATA, SEAFOODMIX
//	}
	
	@Column
	private String steps;
	
	@Column
	private String category;
	
//	public enum Category{
//		VEGETARIAN, CARNIVORE, VEGAN, NUTALLERGY, SHELLFISHALLERGY;
//	}
	
	public Integer getId() {
		return recipe_id;
	}
	public void setId(Integer recipe_id) {
		this.recipe_id = recipe_id; 
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIngredients() {
		return ingredients;
	}
	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}
	public String getSteps() {
		return steps;
	}
	public void setSteps(String steps) {
		this.steps = steps;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	} 

}


