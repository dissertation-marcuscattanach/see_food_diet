package com.see_food_diet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="favourites")
public class Favourite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer favourite_id;

	public Integer getFavourite_id() {
		return favourite_id;
	}

	public void setFavourite_id(Integer favourite_id) {
		this.favourite_id = favourite_id;
	} 

}
