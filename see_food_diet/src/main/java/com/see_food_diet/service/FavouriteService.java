package com.see_food_diet.service;

import java.util.List;

import com.see_food_diet.model.Comment;
import com.see_food_diet.model.Favourite;

public interface FavouriteService {
		void saveAsUserFavourite(Integer favourite_id);
		List<Favourite> listUserFavourites(Integer user_id);
}
