package com.see_food_diet.service;

import java.util.List;

import com.see_food_diet.model.Comment;
import com.see_food_diet.model.Recipe;


public interface CommentService {
	List<Comment> listRecipeComments(Integer recipe_id);
	void saveNewRecipeComment(Integer recipe_id, Comment comment);
}
