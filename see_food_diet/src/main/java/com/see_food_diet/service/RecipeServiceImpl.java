package com.see_food_diet.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.see_food_diet.model.Recipe;
import com.see_food_diet.repository.RecipeRepository;

@Service
public class RecipeServiceImpl implements RecipeService{
	
	@Autowired
	private RecipeRepository recipeRepo;
	
	@Override
	public void saveRecipe(Recipe recipe) {
		this.recipeRepo.save(recipe);		
	}
	
	@Override
	public List<Recipe> getAllRecipes() {
		return recipeRepo.findAll();
	}

	@Override
	public Recipe getRecipeById(Integer recipe_id) {
		Optional<Recipe> optional = recipeRepo.findById(recipe_id);
		Recipe recipe = null; 
		if (optional.isPresent()) {
			recipe = optional.get();			
		}else {
			throw new RuntimeException("Recipe not found for ID " + recipe_id);
		}
		return recipe; 
	}
	
	@Override
	public void deleteRecipeById(Integer id) {
		this.recipeRepo.deleteById(id);
		
	}

}
