package com.see_food_diet.service;

import java.util.List;

import com.see_food_diet.model.Recipe;

public interface RecipeService {
	List<Recipe> getAllRecipes();
	void saveRecipe(Recipe recipe);
	Recipe getRecipeById(Integer recipe_id);
	void deleteRecipeById(Integer recipe_id);
}
