package com.see_food_diet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.see_food_diet.model.Favourite;

public interface FavouriteRepository extends JpaRepository<Favourite, Integer> {

}
