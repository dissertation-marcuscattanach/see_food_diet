package com.see_food_diet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.see_food_diet.model.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Integer>{

}
