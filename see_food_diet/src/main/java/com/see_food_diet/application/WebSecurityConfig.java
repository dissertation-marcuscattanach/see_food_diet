package com.see_food_diet.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.see_food_diet.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl();
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		
		return authProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			//Below are all service requests for Recipe details.
			.antMatchers("/viewLandingPage").permitAll()
			.antMatchers("/saveRecipe").hasAnyAuthority("ADMIN", "USER")
			.antMatchers("/showNewRecipeForm").hasAnyAuthority("ADMIN", "USER")
			.antMatchers("/showFormForUpdate/**").hasAnyAuthority("ADMIN", "USER")
			.antMatchers("/deleteRecipe/**").hasAuthority("ADMIN")
			
			//Below services have not yet been implemented for Recipe. 
			.antMatchers("/listRecipeComments/**").hasAnyAuthority("ADMIN", "USER")
			.antMatchers("/saveNewRecipeComment/**").hasAnyAuthority("ADMIN", "USER")
			.antMatchers("/saveAsUserFavourite/**").hasAnyAuthority("ADMIN", "USER")
			
			//Below are all service requests for User details.
			.antMatchers("/register").permitAll()
			.antMatchers("/process_register").permitAll()
			.antMatchers("/users").hasAuthority("ADMIN")
			
			//Below service has not yet been implemented for User. 
			.antMatchers("/listUserFavourites").hasAnyAuthority("ADMIN", "USER")
			

			
			.and()
			.formLogin().permitAll()
			.and()
			.logout().permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/permission_denied")
			;
	}
}
