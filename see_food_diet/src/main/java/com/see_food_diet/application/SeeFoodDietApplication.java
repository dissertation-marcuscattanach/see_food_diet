package com.see_food_diet.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeeFoodDietApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeeFoodDietApplication.class, args);
	}

}
