package com.see_food_diet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.see_food_diet.model.Recipe;
import com.see_food_diet.model.User;
import com.see_food_diet.service.RecipeService;

@Controller
public class RecipeController {
	
	@Autowired
	private RecipeService recipeService;
	
	@GetMapping("/")
	public String viewLandingPage(Model model) {
		model.addAttribute("listRecipes", recipeService.getAllRecipes());
		return "index";
	}
	 
	@GetMapping("/showNewRecipeForm")
	public String showNewRecipeForm(Model model) {
		// create model attribute to bind form data
		Recipe recipe = new Recipe();
		model.addAttribute("recipe", recipe);
		return "new_recipe";		
		
	}
	
	@PostMapping("/saveRecipe")
	 public String saveRecipe(@ModelAttribute("recipe") Recipe recipe) {
	     // save recipe to database
	     recipeService.saveRecipe(recipe);
	     return "redirect:/";	 
	}
	
	@GetMapping("/showFormForUpdate/{recipe_id}")
	public String showFormForUpdate(@PathVariable(value = "recipe_id") Integer recipe_id, Model model) {
        //Get recipe from the service
        Recipe recipe = recipeService.getRecipeById(recipe_id);
        // Set recipe as a model attribute to pre-populate the form
        model.addAttribute("recipe", recipe);
        return "update_recipe";
    }
	
	@GetMapping("/deleteRecipe/{recipe_id}")
	public String deleteRecipe(@PathVariable (value = "recipe_id") Integer recipe_id) {
	 
		 //Call delete recipe method that is called from Service interface 
		 this.recipeService.deleteRecipeById(recipe_id);
		 return "redirect:/";
	}


}
