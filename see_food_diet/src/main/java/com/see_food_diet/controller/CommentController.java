package com.see_food_diet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import com.see_food_diet.model.Comment;
import com.see_food_diet.model.Recipe;
import com.see_food_diet.service.CommentService;

public class CommentController {
	
	@Autowired
	CommentService commentService; 
	
//	//below is request coming in from html doc
//	@GetMapping("/newRecipeComment/{recipe_id}")
//	//below is method called on from service impl class which implements service interface
//	public String saveNewRecipeComment(@PathVariable(value = "recipe_id") Integer recipe_id, @ModelAttribute("comment") Comment comment) {
//		// save comment to database
//		//Below is invoking above method from CommentService interface
//	    commentService.saveNewRecipeComment(recipe_id, comment);
//	    return "new_comment_form";
//	}
	
//	//Below is taken from the RecipeController as example for saving a new comment above. 
//	@GetMapping("/showNewRecipeForm")
//	public String showNewRecipeForm(Model model) {
//		// create model attribute to bind form data
//		Recipe recipe = new Recipe();
//		model.addAttribute("recipe", recipe);
//		return "new_recipe";
//	}
	


}
