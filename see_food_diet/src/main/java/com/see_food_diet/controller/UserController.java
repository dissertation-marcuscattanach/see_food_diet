package com.see_food_diet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.see_food_diet.model.User;
import com.see_food_diet.model.Role;
import com.see_food_diet.repository.RoleRepository;
import com.see_food_diet.repository.UserRepository;

@Controller
public class UserController {
	
	@Autowired
    private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo; 
	
	@GetMapping("/register")
	public String register(Model model) {
		model.addAttribute("user", new User());
		return "registration_form";
	}
	
	@PostMapping("/process_register")
	public String process_register(User user) {
	    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	    String encodedPassword = passwordEncoder.encode(user.getPassword());
	    user.setPassword(encodedPassword);
	    //Sets enabled to true
	    user.setEnabled(true);
	    Role role = roleRepo.getRoleByName("USER");
	    user.addRole(role);
	    userRepo.save(user);
	     
	    return "successful_registration";
	}
	
	@GetMapping("/users")
	public String users(Model model) {
	    List<User> listUsers = userRepo.findAll();
	    model.addAttribute("listUsers", listUsers);
	    return "users";
	}
	
	@GetMapping("/permission_denied")
		public String error403() {
			return "permission_denied";
	}

}
